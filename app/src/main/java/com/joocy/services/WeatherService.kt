package com.joocy.services

import android.app.IntentService
import android.content.Intent
import android.content.Context
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import kotlin.random.Random

private const val ACTION_FORECAST = "com.joocy.services.action.FORECAST"

private const val EXTRA_LOCATION = "com.joocy.services.extra.LOCATION"
private const val EXTRA_DAY = "com.joocy.services.extra.DAY"

private val forecasts = listOf("Sun", "Rain", "Fog", "Snow")

const val FORECAST = "com.joocy.services.FORECAST"
const val EXTRA_FORECAST = "com.joocy.services.extra.FORECAST"


class WeatherService : IntentService("WeatherService") {

    override fun onHandleIntent(intent: Intent?) {
        when (intent?.action) {
            ACTION_FORECAST -> {
                val location = intent.getStringExtra(EXTRA_LOCATION)
                val day = intent.getStringExtra(EXTRA_DAY)
                Log.i(TAG, "Fetching forecast for $day in $location")
                handleActionForecast(location, day)
            }
        }
    }

    private fun handleActionForecast(location: String, day: String) {
        // we'll just simulate a long-ish network request
        Thread.sleep(Random.nextLong(1000, 3000))
        val intent = Intent(FORECAST).apply {
            putExtra(EXTRA_FORECAST, getForecast(location, day))
        }
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
    }

    private fun getForecast(location: String, day: String): WeatherForecast = WeatherForecast(location, day, forecasts.random())


    companion object {@JvmStatic
        fun startForecast(context: Context, location: String, day: String) {
            val intent = Intent(context, WeatherService::class.java).apply {
                action = ACTION_FORECAST
                putExtra(EXTRA_LOCATION, location)
                putExtra(EXTRA_DAY, day)
            }
            context.startService(intent)
        }

        val TAG = "WeatherService"
    }
}
